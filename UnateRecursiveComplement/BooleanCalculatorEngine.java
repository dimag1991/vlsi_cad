import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

class BooleanCalculatorEngine {

	public static CubeList not(CubeList f) {
		if(f.isEmpty()) {
			return new CubeList(new int[1][f.var_num], f.var_num);
		} else if(f.isContainsAllDoNotCaresCube()) {
			return new CubeList(new int[0][], f.var_num);
		} else if(f.cube_num == 1) {
			ArrayList<Integer> non_do_not_cares_ids = f.non_do_not_cares_ids(0);
			int[][] cubes = new int[non_do_not_cares_ids.size()][f.var_num];
			for(int i = 0; i < cubes.length; i++) {
				cubes[i][non_do_not_cares_ids.get(i)] = (f.cubes[0][non_do_not_cares_ids.get(i)] == 1) ? 2 : 1;
			}
			return new CubeList(cubes, f.var_num);
		}
		int most_binate_id = f.get_splitting_var();
		CubeList P = not(f.cofactor(most_binate_id, 1));
		CubeList N = not(f.cofactor(most_binate_id, -1));
		P = and(most_binate_id, P, 1);
		N = and(most_binate_id, N, -1);
		return or(P, N);
	}
	
	public static CubeList and(CubeList f1, CubeList f2) {
		return not(or(not(f1), not(f2)));
	}
	
	//assumes that f does not contain a var_id variable
	public static CubeList and(int var_id, CubeList f, int pos_neg) {
		int[][] new_cubes = new int[f.cube_num][f.var_num];
		for(int i = 0; i < f.cube_num; i++) {
			for(int j = 0; j < f.var_num; j++) {
				new_cubes[i][j] = f.cubes[i][j];
			}
			new_cubes[i][var_id] = (pos_neg == 1) ? 1:2;
		}
		return new CubeList(new_cubes, f.var_num);
	}
	
	public static CubeList or(CubeList f1, CubeList f2) {
		int cube_num = f1.cube_num + f2.cube_num;
		int var_num = f1.var_num;
		int[][] cubes = new int[cube_num][var_num];
		for(int i = 0; i < f1.cube_num; i++) {
			cubes[i] = f1.cubes[i];
		}
		for(int i = 0; i < f2.cube_num; i++) {
			cubes[i+f1.cube_num] = f2.cubes[i];
		}
		return new CubeList(cubes, f1.var_num);
	}
	
	public static void run(String script) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(script));
		ArrayList<String> commands = new ArrayList<>();
		String line;
		while((line = in.readLine()) != null) {
			commands.add(line);
		}
		in.close();
		int k, n, m;
		HashMap<Integer, CubeList> functions = new HashMap<>();
		for(int i = 0; i < commands.size(); i++) {
			char command = commands.get(i).charAt(0);
			StdOut.println(command);
			if(command == 'q') {
				break;
			}
			String[] tokens = commands.get(i).split("\\s+"); 
			switch(command) {
				case 'r': 
					n = Integer.parseInt(tokens[1]);
					functions.put(n, read_function(n));
					break;
				case '!': 
					k = Integer.parseInt(tokens[1]);
					n = Integer.parseInt(tokens[2]);
					functions.put(k, not(functions.get(n)));
					break;
				case '+': 
					k = Integer.parseInt(tokens[1]);
					n = Integer.parseInt(tokens[2]);
					m = Integer.parseInt(tokens[3]);
					functions.put(k, or(functions.get(n), functions.get(m)));
					break;
				case '&': 
					k = Integer.parseInt(tokens[1]);
					n = Integer.parseInt(tokens[2]);
					m = Integer.parseInt(tokens[3]);
					functions.put(k, and(functions.get(n), functions.get(m)));
					break;
				case 'p': 
					n = Integer.parseInt(tokens[1]);
					write_function(functions.get(n), n + ".pcn"); 
					break;
			}
		}
	}
	
	public static CubeList read_function(int id) throws IOException {
		String file = id + ".pcn";
		BufferedReader in = new BufferedReader(new FileReader(file));
		int var_num = Integer.parseInt(in.readLine().split("\\s+")[0]);
		int cube_num = Integer.parseInt(in.readLine().split("\\s+")[0]);
		int[][] temp = new int[cube_num][var_num];
		int cube = 0;
		String line;
		while((line = in.readLine()) != null) {
			String[] tokens = line.split("\\s+");
			for(int i = 1; i < tokens.length; i++) {
				int token = Integer.parseInt(tokens[i]);
				if(token > 0) {
					temp[cube][token-1] = 1;
				} else {
					temp[cube][-token-1] = 2;
				}
			}
			cube++;
		}
		in.close();
		return new CubeList(temp, var_num);
	}
	
	public static void write_function(CubeList f, String file) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		out.write(f.toString());
		out.close();
	}
	
	public static void main(String[] args) throws IOException  {
		run(args[0]);
	}
}