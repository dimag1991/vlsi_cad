import java.util.ArrayList;

class CubeList {
	
	public int[][] cubes;
	public int var_num;
	public int cube_num;
	
	public CubeList(int[][] cubes, int var_num) {
		cube_num = cubes.length;
		this.var_num = var_num;
		this.cubes = cubes;
	}
	
	public boolean isEmpty() {
		return cube_num == 0;
	}
	
	public int get_splitting_var() {
		int[][] counts = new int[var_num][3];
		for(int i = 0; i < var_num; i++) {
			for(int j = 0; j < cube_num; j++) {
				counts[i][cubes[j][i]]++;
			}
		}
		int split_id = -1;
		if(is_binate_exists(counts)) {
			ArrayList<Integer> most_binate_ids = null;
			int min = Integer.MAX_VALUE;
			for(int i = 0; i < var_num; i++) {
				if(is_binate(counts, i)) {
					if(counts[i][0] < min) {
						min = counts[i][0];
						most_binate_ids = new ArrayList<Integer>();
						most_binate_ids.add(i);
					} else if(counts[i][0] == min) {
						most_binate_ids.add(i);
					}
				}
			}
			if(most_binate_ids.size() == 1) {
				return most_binate_ids.get(0);
			}
			min = Integer.MAX_VALUE;
			for(int id : most_binate_ids) {
				if(Math.abs(counts[id][1] - counts[id][2]) < min) {
					min = Math.abs(counts[id][1] - counts[id][2]);
					split_id = id;
				}
			}
		} else {
			int min = Integer.MAX_VALUE;
			for(int i = 0; i < var_num; i++) {
				if(is_unate(counts, i)) {
					if(counts[i][0] < min) {
						min = counts[i][0];
						split_id = i;
					} 
				}
			}
		}
		return split_id;
	}
	
	public boolean is_binate_exists(int[][] counts) {
		for(int i = 0; i < var_num; i++) {
			if(is_binate(counts, i)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean is_binate(int[][] counts, int id) {
		return (counts[id][1] != 0) && (counts[id][2] != 0);
	}
	
	public boolean is_unate(int[][] counts, int id) {
		return (counts[id][1] != 0) ^ (counts[id][2] != 0);
	}
	
	public CubeList cofactor(int var_id, int pos_neg) {
		int[][] new_cubes;
		int new_cube_num = 0;
		if(pos_neg == 1) {
			for(int i = 0; i < cube_num; i++) {
				if(cubes[i][var_id] != 2) {
					new_cube_num++;
				}
			}
			new_cubes = new int[new_cube_num][var_num];
			int count = 0;
			for(int i = 0; i < cube_num; i++) {
				if(cubes[i][var_id] == 2) {
					continue;
				} else {
					if(cubes[i][var_id] == 1) {
						for(int k = 0; k < var_num; k++) {
							new_cubes[count][k] = cubes[i][k];
						}
						new_cubes[count][var_id] = 0;
					} else {
						new_cubes[count] = cubes[i];
					}
					count++;
				}
			}
		} else {
			for(int i = 0; i < cube_num; i++) {
				if(cubes[i][var_id] != 1) {
					new_cube_num++;
				}
			}
			new_cubes = new int[new_cube_num][var_num];
			int count = 0;
			for(int i = 0; i < cube_num; i++) {
				if(cubes[i][var_id] == 1) {
					continue;
				} else {
					if(cubes[i][var_id] == 2) {
						for(int k = 0; k < var_num; k++) {
							new_cubes[count][k] = cubes[i][k];
						}
						new_cubes[count][var_id] = 0;
					} else {
						new_cubes[count] = cubes[i];
					}
					count++;
				}
			}
		}
		return new CubeList(new_cubes, var_num);
	}
	
	public ArrayList<Integer> non_do_not_cares_ids(int cube_id) {
		ArrayList<Integer> non_do_not_cares_ids = new ArrayList<>();
		for(int i = 0; i < var_num; i++) {
			if(cubes[cube_id][i] != 0) {
				non_do_not_cares_ids.add(i);
			}
		}
		return non_do_not_cares_ids;
	}
	
	public boolean isContainsAllDoNotCaresCube() {
		for(int i = 0; i < cube_num; i++) {
			if(allDoNotCares(cubes[i])) {
				return true;
			}
		}
		return false;
	}
	
	public boolean allDoNotCares(int[] cube) {
		for(int i = 0; i < var_num; i++) { 
			if(cube[i] != 0) {
				return false;
			}
		}
		return true;
	}
	
	public String toString() {
		String s = "";
		s += var_num + "\n" + cube_num + "\n";
		for(int i = 0; i < cube_num; i++) {
			String cube = "";
			int count = 0;
			for(int j = 0; j < var_num; j++) {
				if(cubes[i][j] == 1) {
					cube += (j+1) + " ";
				} else if(cubes[i][j] == 2) {
					cube += (-j-1) + " ";
				} else {
					count++;
				}
			}
			cube = (var_num-count) + " " + cube;
			s += cube + "\n";
		}
		return s;
	}
}