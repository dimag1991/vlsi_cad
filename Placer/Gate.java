class Gate {
	
	public int id;
	public double[] x = new double[2];
	
	public Gate(int id, double x, double y) {
		this.id = id;
		this.x[0] = x;
		this.x[1] = y;
	}
}