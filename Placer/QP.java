import java.io.*;
import java.util.HashSet;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Arrays;
import java.util.Comparator;

class QP {
	
	private double[] gates_x;
	private double[] gates_y;
	private TreeMap<Integer, Double>[] adj_gates;
	private HashMap<Integer, Double>[] adj_pins;
	private double[] pins_x;
	private double[] pins_y;
	private int gate_num;
	
	public QP(String file) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));
		String[] line = in.readLine().split("\\s+");
		int gate_num = Integer.parseInt(line[0]);
		int net_num = Integer.parseInt(line[1]);
		this.gate_num = gate_num;
		gates_x = new double[gate_num];
		gates_y = new double[gate_num];		
		adj_gates = (TreeMap<Integer, Double>[]) new TreeMap[gate_num];
		for(int i = 0; i < gate_num; i++) {
			adj_gates[i] = new TreeMap<>();
		}
		adj_pins = (HashMap<Integer, Double>[]) new HashMap[gate_num];
		for(int i = 0; i < gate_num; i++) {
			adj_pins[i] = new HashMap<>();
		}
		HashSet<Integer>[] nets = (HashSet<Integer>[]) new HashSet[net_num];
		for(int i = 0; i < net_num; i++) {
			nets[i] = new HashSet<>();
		}
		for(int i = 0; i < gate_num; i++) {
			line = in.readLine().split("\\s+");			
			for(int k = 2; k < line.length; k++) {
				int net = Integer.parseInt(line[k]) - 1;
				nets[net].add(i);
			}	
		}		
		int[] nets_pad = new int[net_num];
		for(int i = 0; i < net_num; i++) {
			nets_pad[i] = -1;
		}
		line = in.readLine().split("\\s+");
		int pad_num = Integer.parseInt(line[0]);
		pins_x = new double[pad_num];
		pins_y = new double[pad_num];
		for(int i = 0; i < pad_num; i++) {
			line = in.readLine().split("\\s+");
			int net = Integer.parseInt(line[1]) - 1;
			pins_x[i] = Double.parseDouble(line[2]);
			pins_y[i] = Double.parseDouble(line[3]);
			nets_pad[net] = i;
		}
		for(int i = 0; i < net_num; i++) {
			HashSet<Integer> net = nets[i];
			int pad = nets_pad[i];
			if(pad < 0 && net.size() <= 1) {
				continue;
			}  
			double weight = pad >= 0 ? 1.0/net.size() : 1.0/(net.size()-1);
			for(int gate : net) {
				if(pad >= 0) {
					adj_pins[gate].put(pad, weight);
				}
				for(int temp : net) {
					if(temp != gate) {
						if(!adj_gates[gate].containsKey(temp)) {
							adj_gates[gate].put(temp, weight);
						} else {
							adj_gates[gate].put(temp, adj_gates[gate].get(temp) + weight);
						}
					}
				}
			}
		}			
		in.close();
	}
	
	public void solve(Gate[] gates, int[] index, int lo, int hi, double x_min, double x_max) {
		int size = hi - lo;
		boolean[] is_in_part = new boolean[gate_num];
		for(int i = lo; i < hi; i++) {
			is_in_part[gates[i].id] = true;
		}
		double[] bx = new double[size];
		double[] by = new double[size];
		int nnz = size;
		TreeMap<Integer, Double>[] matrix = (TreeMap<Integer, Double>[]) new TreeMap[size];
		for(int i = 0; i < size; i++) {
			matrix[i] = new TreeMap<>();
		}	
		for(int i = lo; i < hi; i++) {
			double row_sum = 0;
			double x_sum = 0, y_sum = 0;
			for(int gate : adj_gates[gates[i].id].keySet()) {
				row_sum += adj_gates[gates[i].id].get(gate);
				if(is_in_part[gate]) {
					nnz++;
					matrix[i-lo].put(index[gate]-lo, -adj_gates[gates[i].id].get(gate));
				} else {
					x_sum += 50*adj_gates[gates[i].id].get(gate);
					y_sum += gates[index[gate]].x[1]*adj_gates[gates[i].id].get(gate);
				}
			}
			for(int pin : adj_pins[gates[i].id].keySet()) {
				row_sum += adj_pins[gates[i].id].get(pin);
				double pin_x = pins_x[pin];
				if(pin_x > x_max) {
					pin_x = x_max;
				} else if(pin_x < x_min) {
					pin_x = x_min;
				}
				x_sum += pin_x*adj_pins[gates[i].id].get(pin);
				y_sum += pins_y[pin]*adj_pins[gates[i].id].get(pin);
			}
			bx[i-lo] = x_sum;
			by[i-lo] = y_sum;
			matrix[i-lo].put(i-lo, row_sum);
		}
		int[] row = new int[nnz];
		int[] col = new int[nnz]; 
		double[] data = new double[nnz];
		int counter = 0;
		for(int i = 0; i < size; i++) {
			for(int j : matrix[i].keySet()) {
				row[counter] = i;
				col[counter] = j;
				data[counter] = matrix[i].get(j);
				counter++;
			}
		}
		COOMatrix A = new COOMatrix(size, nnz);
		A.setRow(row);
		A.setCol(col);
		A.setData(data);
		double[] x = new double[size];
		double[] y = new double[size];
		A.solve(bx, x);
		A.solve(by, y);
		for(int i = lo; i < hi; i++) {
			gates[i].x[0] = x[i-lo];
			gates[i].x[1] = y[i-lo];
		}
	}
	
	public void place() {
		int left_num = gate_num/2, right_num = gate_num - left_num;
		Gate[] gates = new Gate[gate_num];
		for(int i = 0; i < gate_num; i++) {
			gates[i] = new Gate(i, 0, 0);
		} 
		int[] index = new int[gate_num];
		for(int i = 0; i < gate_num; i++) {
			index[gates[i].id] = i;
		}
		solve(gates, index, 0, gate_num, 0, 100);
		Arrays.sort(gates, new Comparator() {
			public int compare(Object o1, Object o2) {
				double this_key = 100000*((Gate) o1).x[0] + ((Gate) o1).x[1];
				double that_key = 100000*((Gate) o2).x[0] + ((Gate) o2).x[1];
				if(this_key > that_key) {
					return 1;
				} else if(this_key < that_key) {
					return -1;
				}
				return 0;
			}	
		});
		for(int i = 0; i < gate_num; i++) {
			index[gates[i].id] = i;
		}
		solve(gates, index, 0, left_num, 0, 50);
		solve(gates, index, left_num, gate_num, 50, 100);	
		for(int i = 0; i < gate_num; i++) {
			gates_x[gates[i].id] = gates[i].x[0];
			gates_y[gates[i].id] = gates[i].x[1];
		} 
	} 
	
	public void write(String file) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		for(int i = 1; i <= gates_x.length; i++) {
			out.write(String.format(java.util.Locale.UK, "%d %.8f %.8f\n", i, gates_x[i-1], gates_y[i-1]));
		} 
		out.close();
	}
	
	public static void main(String[] args) throws IOException {
		QP qp = new QP(args[0]);
		qp.place();
		qp.write(args[0] + "_out.txt");
	}
}