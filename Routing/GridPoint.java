class GridPoint {
	
	public int x, y, z;
	
	public GridPoint(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public String toString() {
		return z + " " + x + " " + y;
	}
}