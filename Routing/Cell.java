class Cell {
	
	public int cost;
	public boolean is_blocked;
	
	public Cell(int cost, boolean is_blocked) {
		this.cost = cost;
		this.is_blocked = is_blocked;
	}
}