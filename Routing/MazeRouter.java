import java.io.*;

class MazeRouter {
	
	public Net[] nets;
	int layer_num = 2;
	public Cell[][][] grid = new Cell[layer_num][][];
	int net_num, row_num, col_num;
	int bend_penalty, via_penalty;

	public MazeRouter(String grid_file, String netlist_file) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(grid_file));
		String[] line = in.readLine().split("\\s+");
		row_num = Integer.parseInt(line[1]);
		col_num = Integer.parseInt(line[0]);
		bend_penalty = Integer.parseInt(line[2]);
		via_penalty = Integer.parseInt(line[3]);
		for(int k = 0; k < layer_num; k++) {
			grid[k] = new Cell[row_num][col_num];		
			for(int i = 0; i < row_num; i++) {
				line = in.readLine().split("\\s+");
				for(int j = 0; j < col_num; j++) {
					int cost = Integer.parseInt(line[j]);
					boolean is_blocked = (cost == -1) ? true : false;
					grid[k][i][j] = new Cell(cost, is_blocked);
				}
			}
		}
		in.close();
		in = new BufferedReader(new FileReader(netlist_file));
		net_num = Integer.parseInt(in.readLine().split("\\s+")[0]);
		nets = new Net[net_num];
		for(int i = 0; i < net_num; i++) {
			line = in.readLine().split("\\s+");
			int z1 = Integer.parseInt(line[1])-1, x1 = Integer.parseInt(line[2]), y1 = Integer.parseInt(line[3]);
			int z2 = Integer.parseInt(line[4])-1, x2 = Integer.parseInt(line[5]), y2 = Integer.parseInt(line[6]);
			nets[i] = new Net(z1, x1, y1, z2, x2, y2);
		}
		in.close();
	}
	
	public void route() {
		for(int i = 0; i < net_num; i++) {
			nets[i].has_route = run_maze_routing(nets[i]);
		}
	}
	
	public boolean run_maze_routing(Net net) {
		grid[net.z1][net.y1][net.x1].is_blocked = false;
		grid[net.z2][net.y2][net.x2].is_blocked = false;
		int[][][] closed = new int[layer_num][row_num][col_num];
		for(int k = 0; k < layer_num; k++) {
			for(int i = 0; i < row_num; i++) {
				for(int j = 0; j < col_num; j++) {
					closed[k][i][j] = -1;
				}
			}
		}
		MinPQ<SearchNode> fringe = new MinPQ<SearchNode>();
		fringe.insert(new SearchNode(net.x1, net.y1, net.z1, grid[net.z1][net.y1][net.x1].cost, null, (char) 0));
		boolean is_found = false;
		while(true) {
			if(fringe.isEmpty()) {
				grid[net.z1][net.y1][net.x1].is_blocked = true;
				grid[net.z2][net.y2][net.x2].is_blocked = true;
				break;
			}
			SearchNode node = fringe.delMin();
			if(net.x2 == node.x && net.y2 == node.y && net.z2 == node.z) {
				is_found = true;
				while(node != null) {
					grid[node.z][node.y][node.x].is_blocked = true;					
					net.path.push(new GridPoint(node.x, node.y, node.z+1));
					if(node.dir == 'U' || node.dir == 'D') {
						net.path.push(new GridPoint(node.x, node.y, 3));
					}
					node = node.pred;
				}
				break;
			}
			if(closed[node.z][node.y][node.x] == -1 ||  node.cost < closed[node.z][node.y][node.x]) {
				closed[node.z][node.y][node.x] = node.cost;
				String neigh = node.get_2_layer_neighbours();
				for(int i = 0; i < neigh.length(); i++) {
					int new_cost = node.cost;
					char dir = neigh.charAt(i);
					switch(dir) {
						case 'N': {
							if(node.y < row_num-1 && !grid[node.z][node.y+1][node.x].is_blocked) {
								new_cost += (grid[node.z][node.y+1][node.x].cost + ((node.dir == 'S' || node.dir == 0) ? 0 : bend_penalty));
								fringe.insert(new SearchNode(node.x, node.y+1, node.z, new_cost, node, 'S')); 
							}
							break;
						}
						case 'S':  {
							if(node.y > 0 && !grid[node.z][node.y-1][node.x].is_blocked) {
								new_cost += (grid[node.z][node.y-1][node.x].cost + ((node.dir == 'N' || node.dir == 0) ? 0 : bend_penalty));
								fringe.insert(new SearchNode(node.x, node.y-1, node.z, new_cost, node, 'N')); 
							}
							break;
						}
						case 'W':  {
							if(node.x > 0 && !grid[node.z][node.y][node.x-1].is_blocked) {
								new_cost += (grid[node.z][node.y][node.x-1].cost + ((node.dir == 'E' || node.dir == 0) ? 0 : bend_penalty));
								fringe.insert(new SearchNode(node.x-1, node.y, node.z, new_cost, node, 'E')); 
							}
							break;
						}
						case 'E':  {
							if(node.x < col_num-1 && !grid[node.z][node.y][node.x+1].is_blocked) {
								new_cost += (grid[node.z][node.y][node.x+1].cost + ((node.dir == 'W' || node.dir == 0) ? 0 : bend_penalty));
								fringe.insert(new SearchNode(node.x+1, node.y, node.z, new_cost, node, 'W')); 
							}
							break;
						}
						case 'U':  {
							if(!grid[node.z+1][node.y][node.x].is_blocked) {
								new_cost += (grid[node.z+1][node.y][node.x].cost + via_penalty);
								fringe.insert(new SearchNode(node.x, node.y, node.z+1, new_cost, node, 'D')); 
							}
							break;
						}
						case 'D':  {
							if(!grid[node.z-1][node.y][node.x].is_blocked) {
								new_cost += (grid[node.z-1][node.y][node.x].cost + via_penalty);
								fringe.insert(new SearchNode(node.x, node.y, node.z-1, new_cost, node, 'U')); 
							}
							break;
						} 
						default:  break;
					}
				}
			}
		}
		return is_found;
	}
	
	public void write_results(String file) throws IOException {
		StdOut.println("Results writing begins...");
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		out.write(net_num + "\n");
		for(int i = 1; i <= net_num; i++) {
			out.write(i + "\n");
			if(nets[i-1].has_route) {
				for(GridPoint p : nets[i-1].path) {
					out.write(p + "\n");
				}
			}
			out.write(0+"\n");
		}
		out.close();
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		MazeRouter r = new MazeRouter(args[0], args[1]);
		r.route();
		r.write_results("out.txt");
		StdOut.println("\nTiming results: " + sw.elapsedTime());
	}
}