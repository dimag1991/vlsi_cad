class Net {
	
	public int z1, x1, y1, z2, x2, y2;
	public boolean has_route;
	public Stack<GridPoint> path = new Stack<>();
	
	public Net(int z1, int x1, int y1, int z2, int x2, int y2) {
		this.z1 = z1;
		this.x1 = x1;
		this.y1 = y1;
		this.z2 = z2;
		this.x2 = x2;
		this.y2 = y2;
	}
}