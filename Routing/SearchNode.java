class SearchNode implements Comparable<SearchNode> {
	
	public int cost;
	public int x, y, z;
	public char dir;
	public SearchNode pred;

	public SearchNode(int x, int y, int z, int cost, SearchNode pred, char dir) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.dir = dir;
		this.cost = cost;
		this.pred = pred;
	}
	
	public String get_2_layer_neighbours() {
		if(z == 0) {
			return "NSEWU";
		} 	
		return "NSEWD";
	}
	
	public int compareTo(SearchNode that) {
		if(this.cost < that.cost) {
			return -1;
		} else if(this.cost > that.cost) {
			return 1;
		}
		return 0;
	}
}